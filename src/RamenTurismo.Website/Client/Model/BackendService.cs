﻿using RamenTurismo.Website.Shared;

namespace RamenTurismo.Website.Client.Model;

public sealed class BackendService
{
    private readonly HttpClient _client;

    public BackendService(HttpClient client)
    {
        _client = client;
    }

    public Task<IReadOnlyList<BlogPostSummary>> GetBlogPostSummaryAsync(int from, int to)
    {
        return Task.FromResult<IReadOnlyList<BlogPostSummary>>(new BlogPostSummary[]
        {
            new BlogPostSummary
            {
                ChangedAt = DateTime.UtcNow,
                CreatedAt= DateTime.UtcNow,
                ShortDescription = "Testing, for now",
                Slug = "I do not know",
                Tags =
                {
                    "C#",
                    "Security"
                },
                Title = "How to not make an app"
            }
        });
    }
}
