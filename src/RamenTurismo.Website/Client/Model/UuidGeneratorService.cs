﻿namespace RamenTurismo.Website.Client.Model;

public sealed class UuidGeneratorService
{
    public IEnumerable<Guid> Generate(int count)
    {
        return Enumerable.Range(0, count).Select(_ => Guid.NewGuid());
    }
}
