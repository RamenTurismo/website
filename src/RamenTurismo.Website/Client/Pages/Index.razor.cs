﻿using Microsoft.AspNetCore.Components;
using RamenTurismo.Website.Client.Model;
using RamenTurismo.Website.Shared;

namespace RamenTurismo.Website.Client.Pages;

public partial class Index
{
    [Inject]
    public BackendService BackendService { get; set; } = null!;

    private const int MaxPostsPerScroll = 10;

    private readonly List<BlogPostSummary> _posts = new(capacity: MaxPostsPerScroll);

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        // TODO From cache.
        IReadOnlyList<BlogPostSummary> posts = await BackendService.GetBlogPostSummaryAsync(0, MaxPostsPerScroll);

        _posts.AddRange(posts);
    }
}
