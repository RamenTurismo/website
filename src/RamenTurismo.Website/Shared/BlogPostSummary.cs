﻿namespace RamenTurismo.Website.Shared;

public sealed class BlogPostSummary
{
    /// <summary>
    /// Human readable ID.
    /// </summary>
    public string Slug { get; init; } = null!;

    public string Title { get; init; } = null!;

    public DateTime CreatedAt { get; init; }

    public DateTime? ChangedAt { get; init; }

    public string ShortDescription { get; init; } = null!;

    public ICollection<string> Tags { get; }

    public BlogPostSummary()
    {
        Tags = new HashSet<string>();
    }
}
