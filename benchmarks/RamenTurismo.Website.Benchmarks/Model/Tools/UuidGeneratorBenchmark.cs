﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;
using System.Linq;

namespace RamenTurismo.Website.Benchmarks.Model.Tools;

[MinColumn, MaxColumn]
[MemoryDiagnoser(displayGenColumns: false)]
public class UuidGeneratorBenchmark
{
    [Params(1, 10, 100, 300)]
    public int Count { get; set; }

    [Benchmark(Baseline = true)]
    public Guid[] Original()
    {
        return Enumerable.Range(0, Count).Select(_ => Guid.NewGuid()).ToArray();
    }

    [Benchmark]
    public Guid[] ForLoop()
    {
        return ForLoopStatic().ToArray();
    }

    [Benchmark]
    public Task<Guid[]> ForLoopAsync()
    {
        return Task.WhenAll(ForLoopStaticAsync());
    }

    [Benchmark]
    public Task<Guid[]> EnumerableAsync()
    {
        return Task.WhenAll(Enumerable.Range(0, Count).Select(_ => CreateTask()));
    }

    private IEnumerable<Guid> ForLoopStatic()
    {
        for (int i = 0; i < Count; i++)
        {
            yield return Guid.NewGuid();
        }
    }

    private IEnumerable<Task<Guid>> ForLoopStaticAsync()
    {
        for (int i = 0; i < Count; i++)
        {
            yield return CreateTask();
        }
    }

    private static Task<Guid> CreateTask()
    {
        TaskCompletionSource<Guid> tcs = new(TaskCreationOptions.RunContinuationsAsynchronously);

        Task.Factory.StartNew(state => 
        {
            ((TaskCompletionSource<Guid>)state!).TrySetResult(Guid.NewGuid());
        }, tcs, TaskCreationOptions.LongRunning);

        return tcs.Task;
    }
}
